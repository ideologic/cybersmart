// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.

window.addEventListener("DOMContentLoaded", () => {
  const replaceText = (selector, text) => {
    const element = document.getElementById(selector);
    if (element) element.innerText = text;
  };

  const refreshCpuTemp = () => {
    const si = require("systeminformation");
    si.cpuTemperature()
      .then((data) => {
        const cpuTemp = data && data.main ? `${Math.round(data.main * 10) / 10}°C` : "unavailable";
        replaceText("temp-cpu", cpuTemp);
      })
      .catch((error) => {
        replaceText("temp-cpu", "unavailable (error occurred)");
        console.error(error);
      });
  };

  const refreshOutsideTemp = () => {
    fetch("https://api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=697dddebc62e1bdb3dc072a82325aa70")
      .then((response) => response.json())
      .then((data) => {
        const outsideTemp = data && data.main && data.main.temp ? `${Math.round((data.main.temp - 273.15) * 10) / 10}°C` : "unavailable";
        replaceText("temp-outside", outsideTemp);
      })
      .catch((error) => {
        console.error(error);
        replaceText("temp-outside", "unavailable (error occurred)");
      });
  };

  refreshCpuTemp();
  refreshOutsideTemp();
});
